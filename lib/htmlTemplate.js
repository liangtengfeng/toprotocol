const fs = require('fs');
const CONTENT_FLAG = '#content#';
const path = require('path');



const getCss = function() {
  return fs.readFileSync(path.resolve(__dirname, './protocol.css'),'utf-8').toString();
}


const template = `<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title></title>
  <style>
    #css#
  </style>
</head>
<body>
    <div id="app">${CONTENT_FLAG}</div>
</body>
</html>`;


const getDyTemplate = function() {
    return template.replace('#css#',getCss())
}


module.exports = {
	template: template.replace('#css#',getCss()),
	CONTENT_FLAG,
  getDyTemplate
};
