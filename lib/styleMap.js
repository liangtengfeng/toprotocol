const { transformDocument, PARAGRAPH_INDENTNAME, TEXT_INDENTNAME } = require('./transformDocument.js');

const options = {
	styleMap: [
		"p[style-name='Heading 1'] => h1.head1:fresh",
		"p[style-name='Heading 2'] => h2.head2:fresh",
		"p[style-name='Heading 3'] => h3.head3:fresh",
		"p[style-name^='Normal'] => p:fresh",
		`p[style-name='${PARAGRAPH_INDENTNAME}'] => div.paragraph-indent:fresh`,
		`p[style-name='${TEXT_INDENTNAME}'] => div.text-indent:fresh`,
    "r[style-name='TEXT_UNDENLINE'] => span.text-underline:fresh"
	],
	transformDocument: transformDocument
};


module.exports = options
