
# docx2html
根据`mammoth`进行二次开发,旨在将`docx`格式的协议文件转为`html`文件


## 安装
```shell
npm install @utils/docx2html
```

## 使用介绍
```shell
usage: docx2html [-h] [-v] [-w] docx-path [output-path]

positional arguments:
  docx-path      Path to the .docx file to convert.
  output-path    Output path for the generated document. Images will be stored inline in the output document. Output is written to stdout if not set.

optional arguments:
  -h, --help     show this help message and exit
  -v, --version  show program's version 
  -w, --watch    enter watching mode
```


### 用法1
直接把结果输出在终端上

```shell
  docx2html demo.docx
```

### 用法2
把结果保存在`demo.html`
```shell
  docx2html demo.docx  demo.html
```


### 用法3
加上参数`-w`,本地起服务，实时监听`docx`文件变化从而刷新页面,保存在`demo.html`
```shell
  docx2html demo.docx  demo.html -w
```



## 转化规则
所有`px`单位按设计稿比例转化为`vw`单位

### 容器
协议内容被套在里面`<div id="#app">协议内容</div>`
```scss
#app{
    margin: 0 pxToVW(60) pxToVW(183);
    color: rgba(0, 0, 0, 0.45);
    font-size: pxToVW(42);
}
```
### 主标题
将`docx`文档中`标题 1`样式，转化成`<h1 class="head1">主标题</h1>`
```scss
.head1 {
  color: rgba(0, 0, 0, 0.9);
  font-size: pxToVW(72);
  line-height: 1.17;
  text-align: center;
  margin: pxToVW(156) 0 pxToVW(108);
}
```
![demo](https://gitee.com/liangtengfeng/toprotocol/raw/master/images/demo.png)
### 二级标题
将`docx`文档中`标题 2`样式，转化成`<h2 class="head2">二级标题</h1>`
```scss
.head2 {
  color: rgba(0, 0, 0, 0.9);
  font-size: pxToVW(45);
  line-height: 1.47;
  text-align: left;
  margin: pxToVW(84) 0 pxToVW(30);
}
```

### 正文内容
所有段落都转成`p`标签包裹
```scss
#app>p {
  margin: 0 0 pxToVW(24) 0;
  line-height: 1.64;
  text-align: left;
}
```

### 整段落缩进
有缩进的段落会转换为`<div class="paragraph-indent"></div>`
```scss
.paragraph-indent {
  margin-left: 1em;
  & > p {
    &:first-child{
      margin-top: - pxToVW(12);
    }
    margin: 0 0 pxToVW(24) 0;
    line-height: 1.64;
    text-align: left;
    margin-bottom: pxToVW(12);
  }
}
```


### 加粗
有加粗样式的都会转为`<strong>需要加粗的文字</strong>`

```scss
strong{
  color: rgba(0, 0, 0, 0.9);
}
```

### 下划线
有下划线样式的都会转为`<span class="text-underline">需要有下划线的内容</span>`

```scss
.text-underline{
  text-decoration: underline;
}
```


### 底部备注内容
只提供样式，不会自动转换标签，一般用于更新时间等等
```scss
.footer-title {
  color: rgba(0,0,0,0.25);
  font-size: pxToVW(36);
  line-height: 1.17;
  margin-top: pxToVW(36);
}
```
